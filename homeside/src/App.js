import React, { Component } from 'react';



const API_KEY = process.env.REACT_APP_AIRTABLE_API_KEY
const URL = `https://api.airtable.com/v0/appBEFGRIZT2HqMkX/favorites?api_key=${API_KEY}`



class App extends Component {

  constructor(props) {
    super(props);
    this.state = {
      movies: [],
      filter: "",

    }

    this.handleFilter = this.handleFilter.bind(this);
  }

  refresh() {
    setTimeout(() => {  window.location.reload(); }, 900000);
  }
  handleFilter(event) {
    const value = event.target.value;
    this.setState({ filter: value })
  }

  async componentDidMount() {
      fetch(URL)
      .then((resp) => resp.json())
      .then(data => {
        console.log(data);
        this.setState({ movies: data.records.reverse() });
      }).catch(err => {
        // Error
      });
      this.refresh();
  }


  render() {
    return (
      <>
      {/* <div className="container mt-5">
        <div className="row">
          <div className="col">
            <div className="card-deck">
              {this.state.movies.map(movie => <MovieCard {...movie.fields} key={movie.fields.title} /> )}
            </div>
          </div>
        </div>
      </div> */}
      
      <h1 className='text-center'> LOAN OFFICER TABLE</h1>
      
      <div className='container'>
    <div className="row">
    <div className="col-12">
    <div className="input-group mb-3 ">
                <input onChange={this.handleFilter} type="text" className="form-control" placeholder="SEARCH BY AGENT ID" />
            </div>
  <table className="table table-striped  table-hover" id="sortTable"> 
  
    <thead>
      <tr>
        <th className='text-center'scope='col'> Agent ID</th>
        <th className='text-center'scope='col'> Name</th>
        <th className='text-center' scope='col'> Email</th>
        <th className='text-center' scope='col'> Phone</th>
        
        {/* <th scope='col'> IMAGE</th> */}
      </tr>
    </thead>
    <tbody>
      
    {this.state.movies.map(movie => {
                      let toggle = ""
                      if (movie.fields.id  !== this.state.filter && this.state.filter !== "") {
                        toggle = "d-none" }
                        
      return (
        <tr key={movie.fields.id} className={toggle}>
          <td className='text-center'> {movie.fields.id} </td>
        <td className='text-center'> {movie.fields.name} </td>
        <td className='text-center'> {movie.fields.email} </td>
        <td className='text-center'> {movie.fields.phone} </td>
       
        {/* <td className='w-25'> 
          <img src={movie.fields.imageURL[0].url} className="img-fluid img-thumbnail" alt="" />
          </td> */}
        </tr> 
      );
    } )}
    
    </tbody>
  </table>
  
  </div>
  </div>
  </div>
      </>
    );
  }
}

export default App;

const MovieCard = ({ title, year, description, imageURL }) => (
  <div className="card">
    <img className="card-img-top" src={imageURL[0].url} alt="Movie poster" />
    <div className="card-body">
      <h5 className="card-title">{title}</h5>
      <p className="card-text">{description}</p>
      <p className="card-text">
        <small className="text-muted">{year}</small>
      </p>
    </div>
  </div>
);


 

